package com.codeclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CakeStoreApp {

	public static void main(String... args) {
		SpringApplication.run(CakeStoreApp.class, args);
	}

}
